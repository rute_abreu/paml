from unittest import TestCase

from sklearn.datasets import make_regression, make_classification
from sklearn.ensemble import RandomForestRegressor, AdaBoostClassifier, AdaBoostRegressor
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVR

from sklearn.metrics import roc_auc_score

from xgboost import XGBClassifier, XGBRegressor
from lightgbm import  LGBMClassifier, LGBMRegressor

import time
from paml.searches.OptunaSearch import OptunaSearch
from paml.searches.wrapper_catboost import CatBoostClassifierScikit, CatBoostRegressorScikit

class TestOptunaSearch(TestCase):
    def run_task(self, n_trials, model_alias, task):
        data = make_classification(n_samples = 100, n_features = 8)
        if task == 'regression':
            data = make_regression(n_samples = 100, n_features = 8)       
        X = data[0]
        y = data[1]

        if model_alias == 'decision_tree':
            y = y**2

        Optuna = OptunaSearch(n_trials = n_trials, task = task, 
                                        compute_ks = False, n_folds = 5,
                                        models = [model_alias],
                                        feature_selection=False)
        before = time.time()
        best_model = Optuna.fit(X, y)
        print('Run in: ', (time.time() - before)/60, 'min')
        return best_model

    def test_catboost_clf(self):
        model = self.run_task(1, 'catboost', 'classification')
        self.assertIsInstance(model.named_steps['estimator'].model, CatBoostClassifierScikit)

    def test_catboost_rgs(self):
        model = self.run_task(1, 'catboost', 'regression')
        self.assertIsInstance(model.named_steps['estimator'].model, CatBoostRegressorScikit)

    def test_lgbm_clf(self):
        model = self.run_task(1, 'lgbm', 'classification')
        self.assertIsInstance(model.named_steps['estimator'].model, LGBMClassifier)

    def test_lgbm_rgs(self):
        model = self.run_task(1, 'lgbm', 'regression')
        self.assertIsInstance(model.named_steps['estimator'].model, LGBMRegressor)

    def test_xgb_clf(self):
        model = self.run_task(1, 'xgb', 'classification')
        self.assertIsInstance(model.named_steps['estimator'].model, XGBClassifier)

    def test_xgb_rgs(self):
        model = self.run_task(1, 'xgb', 'regression')
        self.assertIsInstance(model.named_steps['estimator'].model, XGBRegressor)

    def test_adaboost_clf(self):
        model = self.run_task(10, 'adaboost', 'classification')
        self.assertIsInstance(model.named_steps['estimator'].model, AdaBoostClassifier)

    def test_adaboost_rgs(self):
        model = self.run_task(1, 'adaboost', 'regression')
        self.assertIsInstance(model.named_steps['estimator'].model, AdaBoostRegressor)

    def test_decision_tree_clf(self):
        model = self.run_task(1, 'decision_tree', 'classification')
        self.assertIsInstance(model.named_steps['estimator'].model, DecisionTreeClassifier)

    def test_decision_tree_rgs(self):
        model = self.run_task(1, 'decision_tree', 'regression')
        self.assertIsInstance(model.named_steps['estimator'].model, DecisionTreeRegressor)

    def test_svr_clf(self):
        model = self.run_task(1, 'logistic_regressor', 'classification')
        self.assertIsInstance(model.named_steps['estimator'].model, LogisticRegression)

    def test_svr_clf(self):
        model = self.run_task(1, 'svr', 'regression')
        self.assertIsInstance(model.named_steps['estimator'].model, SVR)