from unittest import TestCase

from sklearn.ensemble import AdaBoostClassifier, AdaBoostRegressor
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVR

from sklearn.datasets import make_regression, make_classification

from catboost.core import CatBoostClassifier, CatBoostRegressor
from lightgbm.sklearn import LGBMClassifier, LGBMRegressor
from xgboost import XGBClassifier, XGBRegressor

from paml.searches import GridSearch
import time
import numpy as np

from paml.searches.wrapper_catboost import CatBoostClassifierScikit, CatBoostRegressorScikit

class TestsGridSearch(TestCase):

    def create_full_grid_classification(self):
        search_space = [
        {'estimator': ['xgb_clf'],
                    'eval_metric': ["auc"],
                    },
                    {'estimator': ['catboost_clf'],
                    'n_estimators'           : [100],
                    'early_stopping_rounds'  : [50],},
                    {'estimator': ['adaboost_clf'],},
                    {'estimator': ['lgbm_clf'],
                    'objective': ['binary'],
                    'num_iterations': [100],
                    'metric': ["auc"]},
                    {'estimator': ['logistic_clf'],
                    'solver' : ['newton-cg'],
                    'C': [1]}]


        feature_selector_search_space = [{'max_depth': [1]}]


        gridSearch = GridSearch(task = 'classification', 
                                search_space = search_space, 
                                feature_selector_search_space=feature_selector_search_space)

        data = make_classification(n_samples = 10, n_features = 4)       
        X = data[0]
        y = data[1]
        model = gridSearch.run_grid_search_classification(X,y)

        self.assertIsInstance(gridSearch, GridSearch)
 
    def create_full_grid_regression(self):
        search_space =  [{'estimator': ['xgb_rgs'],
                    'learning_rate': [0.3]},

                    {'estimator': ['catboost_rgs'],},

                    {'estimator': ['adaboost_rgs'],
                    'n_estimators': [10],
                    'loss': ["linear"],
                    'learning_rate': [0.3],},

                    {'estimator': ['lgbm_rgs'],
                    'objective': ['regression'],
                    'n_estimators': [10],
                    'metric': ["R2"],
                    'random_state': [42],
                    'early_stopping_round': [5],
                    'boosting': ['gbdt'],
                    'num_leaves':[2],
                    'learning_rate': [1e-5],
                    'max_depth': [2],
                    'min_data_in_leaf': [10],
                    'min_sum_hessian_in_leaf': [1e-3],
                    'feature_fraction': [0.7],
                    'feature_fraction_bynode': [0.4],
                    'min_data_per_group': [50],
                    'bagging_freq': [5],
                    'bagging_fraction': [0.3],
                    'cat_l2': [10],
                    'categorical_feature': ['auto'],
                    'cat_smooth': [10],},
                    
                    {'estimator': ['svr_rgs'],
                    'kernel': ["linear"],
                    'gamma':["scale"],
                    'C':[1],
                    'epsilon': [1e-2]}]

        feature_selector_search_space = [{'max_depth': [1]}]


        gridSearch = GridSearch(target_name = 'y', task = 'regression', 
                        search_space = search_space, 
                        feature_selector_search_space=feature_selector_search_space)

        data = make_regression(n_samples = 10, n_features = 4)       
        X = data[0]
        y = data[1]
        model = gridSearch.run_grid_search_regression(X,y)

    def classify_task(self, model_alias):
        data = make_classification(n_samples = 200, n_features = 4)       
        X = data[0]
        y = data[1]


        search_space = [{'estimator': [model_alias],
                    # 'eval_metric': ["AUC"],
                    
                    }]

        # if model_alias == 'xgb':
        #     search_space[0]['eval_metric'] = ['auc']

        feature_selector_search_space = [{'max_depth': [5]}]
        gridSearch = GridSearch(task = 'classification',
                                models = [model_alias],
                                search_space = search_space,
                                feature_selector_search_space= feature_selector_search_space,
                                compute_ks=True, feature_selection=True)
        before = time.time()
        model = gridSearch.run_grid_search_classification(X,y)
        print('Run in: ', (time.time() - before)/60, 'min')
        return model

    def regression_task(self, model_alias):
        data = make_regression(n_samples = 200, n_features = 6)       
        X = data[0]
        y = data[1]

        search_space = [{'estimator': [model_alias]}]
        feature_selector_search_space = [{'max_depth': [1]}]
        gridSearch = GridSearch(task = 'regression',
                                models = [model_alias],
                                search_space = search_space, 
                                feature_selector_search_space=feature_selector_search_space, feature_selection=True)
        before = time.time()
        model = gridSearch.run_grid_search_regression(X,y)
        print('Ran in: ', (time.time() - before)/60, 'min')
        return model


    def test_xgboost_rgs_tunning(self):
       model = self.regression_task('xgb')
       self.assertIsInstance(model.named_steps['estimator'].model, XGBRegressor)

    def test_xgboost_clf_tunning(self):
        model = self.classify_task('xgb')
        self.assertIsInstance(model.named_steps['estimator'].model, XGBClassifier)

    def test_catboost_rgs(self):
        model = self.regression_task('catboost')
        self.assertIsInstance(model.named_steps['estimator'].model, CatBoostRegressorScikit)
    def test_catboost_clf(self):
        model = self.classify_task('catboost')
        self.assertIsInstance(model.named_steps['estimator'].model, CatBoostClassifierScikit)

    def test_adabboost_rgs(self):
        model = self.regression_task('adaboost')
        self.assertIsInstance(model.named_steps['estimator'].model, AdaBoostRegressor)
    def test_adaboost_clf(self):
        model = self.classify_task('adaboost')
        self.assertIsInstance(model.named_steps['estimator'].model, AdaBoostClassifier)

    def test_lgbm_rgs(self):
        model = self.regression_task('lgbm')
        self.assertIsInstance(model.named_steps['estimator'].model, LGBMRegressor)

    def test_lgbm_clf(self):
        model = self.classify_task('lgbm')
        self.assertIsInstance(model.named_steps['estimator'].model, LGBMClassifier)

    def test_decision_tree_clf(self):
        model = self.classify_task('decision_tree')
        self.assertIsInstance(model.named_steps['estimator'].model, DecisionTreeClassifier)

    def test_decision_tree_rgs(self):
        model = self.regression_task('decision_tree')
        self.assertIsInstance(model.named_steps['estimator'].model, DecisionTreeRegressor)

    def test_svr_rgs(self):
        model = self.regression_task('svr')
        self.assertIsInstance(model.named_steps['estimator'].model, SVR)

    def test_logistic(self):
        model = self.classify_task('logistic_regressor')
        self.assertIsInstance(model.named_steps['estimator'].model, LogisticRegression)
