
from unittest import TestCase
import pandas as pd
from sklearn.datasets import make_regression, make_classification
from sklearn.model_selection import GridSearchCV as GridCV
from sklearn.ensemble import RandomForestRegressor, AdaBoostClassifier, AdaBoostRegressor
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVR
from sklearn.metrics import roc_auc_score

from xgboost import XGBClassifier, XGBRegressor
from lightgbm import  LGBMClassifier, LGBMRegressor
from catboost import CatBoostClassifier, CatBoostRegressor, cv

from paml.searches import GridSearchCV
import time

from paml.searches.wrapper_catboost import CatBoostClassifierScikit, CatBoostRegressorScikit
class TestGridSearchCV(TestCase):

    def test_grid_creation(self):
        search = GridSearchCV()
        self.assertIsInstance(search, GridSearchCV)

    def run_task(self, model_alias, task, search_space):
        data = make_classification(n_samples = 200, n_features = 4)

        # titanic_train_df = pd.read_csv("/home/ruteabreu/git/automl/paml/tests/train.csv")
        # X = titanic_train_df.drop(['Survived', 'Name'], axis = 1)
        # y = titanic_train_df['Survived']
        if task == 'regression':
            data = make_regression(n_samples = 200, n_features = 4)
            X = data[0]
            y = data[1]

        X = data[0]
        y = data[1]

        if task == 'classification':
            compute_ks = True
        else:
            compute_ks = False
        gridSearch = GridSearchCV(task = task, n_jobs = -1, n_folds = 2, compute_ks = compute_ks,
                                 models = [model_alias], search_space = search_space, acceptance_rate=0.01)
        before = time.time()
        model = gridSearch.fit_model(X, y)
        print('Ran in: ', (time.time() - before)/60, 'min')
        return model

    def test_catboost_clf(self):
        search_space = [{'estimator': ['catboost']}]
        model = self.run_task('catboost', 'classification', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['clf'], CatBoostClassifierScikit)

    def test_catboost_rgs(self):
        search_space = [{'estimator': ['catboost'],}]
        model = self.run_task('catboost', 'regression', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['rgs'], CatBoostRegressorScikit)

    def test_xgb_clf(self):
        search_space = [{'estimator': ['xgb']}]
        model = self.run_task('xgb', 'classification', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['clf'], XGBClassifier)

    def test_xgb_rgs(self):
        search_space = [{'estimator': ['xgb'],}]
        model = self.run_task('xgb', 'regression', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['rgs'], XGBRegressor)
        
    
    def test_adaboost_rgs(self):
        search_space = [{'estimator': ['adaboost'],}]
        model = self.run_task('adaboost', 'regression', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['rgs'], AdaBoostRegressor)

    def test_adaboost_clf(self):
        search_space = [{'estimator': ['adaboost'],}]
        model = self.run_task('adaboost', 'classification', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['clf'], AdaBoostClassifier)

    def test_lgbm_clf(self):
        search_space = [{'estimator': ['lgbm']}]
        model = self.run_task('lgbm', 'classification', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['clf'], LGBMClassifier)

    def test_lgbm_rgs(self):
        search_space = [{'estimator': ['lgbm'],}]
        model = self.run_task('lgbm', 'regression', search_space).best_estimator_                   
        self.assertIsInstance(model.named_steps['rgs'], LGBMRegressor)

    def test_decision_tree_clf(self):
        search_space = [{'estimator': ['decision_tree'],}]
        model = self.run_task('decision_tree', 'classification', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['clf'], DecisionTreeClassifier)

    def test_decision_tree_rgs(self):
        search_space = [{'estimator': ['decision_tree'],}]
        model = self.run_task('decision_tree', 'regression', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['rgs'], DecisionTreeRegressor)

    def test_logistic_regression_clf(self):
        search_space = [{'estimator': ['logistic_regressor'],}]
        model = self.run_task('logistic_regressor', 'classification', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['clf'], LogisticRegression)

    def test_svr_rgs(self):
        search_space = [{'estimator': ['svr'],}]
        model = self.run_task('svr', 'regression', search_space).best_estimator_
        self.assertIsInstance(model.named_steps['rgs'], SVR)