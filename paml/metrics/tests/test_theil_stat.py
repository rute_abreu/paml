from unittest import TestCase
from paml.metrics import Metrics

import numpy as np
class TestTheilStat(TestCase):
    def test_theil_stat(self):
        y_pred = np.random.random(size = 3)
        y_true = np.ones(3)
        theil_index = Metrics.__theil_stat__(y_true, y_pred)

        summation = 0
        for y_t, y_p in zip(y_true, y_pred):
            summation += (y_p - y_t)**2
       
        mean_denominator = summation/(len(y_true))

        theil_index_alt = (np.mean(y_pred) - np.mean(y_true))/mean_denominator

        print(theil_index_alt, theil_index)

        self.assertEqual(theil_index, theil_index_alt)




