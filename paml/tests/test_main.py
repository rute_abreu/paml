from unittest import TestCase
from boruta.boruta_py import BorutaPy
from lightgbm.sklearn import LGBMRegressor
from sklearn.ensemble import AdaBoostClassifier, RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from xgboost import XGBRegressor, XGBClassifier
from lightgbm import LGBMClassifier
from sklearn.datasets import make_regression, make_classification, load_iris
import pandas as pd
from paml.automl import AutoML


import time

from paml.searches.wrapper_catboost import CatBoostClassifierScikit, CatBoostRegressorScikit

class TestMain(TestCase):

    def test_normal_flow_one_model_gs_regressor(self):
        search_space = [{'estimator': ['catboost'], 'eval_metric': ['RMSE']}]

        autoML = AutoML(search = 'GridSearch', task = 'regression', models=['catboost'], 
                        search_space = search_space, feature_selection = True)

        data = make_regression(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        X = pd.DataFrame(data = X, columns = ['a','b', 'c', 'd'])

        model = autoML.fit(X, y)
        self.assertIsInstance(model.named_steps['estimator'].model, CatBoostRegressorScikit)
   

    def test_normal_flow_one_model_gs_classification(self):
        search_space = [{'estimator': ['logistic_regression']}]

        autoML = AutoML(search = 'GridSearch', task = 'classification', models=['logistic_regression'], 
                        search_space = search_space, feature_selection = True)

        

        # titanic_train_df = pd.read_csv("/home/ruteabreu/git/automl/paml/tests/train.csv")
        # X = titanic_train_df.drop(['Survived'], axis = 1)
        # y = titanic_train_df['Survived']


        data = make_classification(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        # data = load_iris()
        # X = data['data']
        # y = data['target']


        model = autoML.fit(X, y)
        self.assertIsInstance(model.named_steps['estimator'].model, LogisticRegression)

    def test_flow_model_gs_regressor_type1(self):
        '''Type 1 - set a search space and a model that is not in the search space'''

        search_space = [{'estimator': ['catboost'],
                        'eval_metric': ["RMSE"],}]

        autoML = AutoML(search = 'GridSearch', task = 'regression', models=['xgb'], 
                        search_space = search_space)

        data = make_regression(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        before = time.time()
        model = autoML.fit(X, y)
        print("Ran in ", (time.time() - before)/60)
        self.assertIsInstance(model.named_steps['estimator'].model, XGBRegressor)

    def test_flow_model_gs_regressor_type2(self):
        '''Type 2 - set just a model'''

        autoML = AutoML(search = 'GridSearch', task = 'regression', models=['catboost'])

        data = make_regression(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        model = autoML.fit(X, y)
        self.assertIsInstance(model.named_steps['estimator'].model, CatBoostRegressorScikit)

    def test_flow_model_gs_regressor_type3(self):
        '''Type 3 - set two or more models without a search_space'''

        autoML = AutoML(search = 'GridSearch', task = 'regression', models=['catboost', 'lgbm'])

        data = make_regression(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        model = autoML.fit(X, y)
        

    def test_flow_model_gs_regressor_type4(self):
        '''Type 4 - set two or more models with a partially correct search_space'''
        search_space = [{'estimator': ['catboost'],
                        'eval_metric': ["RMSE"],},
                        {'estimator': ['xgb']}]

                        
        autoML = AutoML(search = 'GridSearch', task = 'regression', models=['catboost', 'lgbm'], 
                        search_space = search_space)

        data = make_regression(n_features=4, n_samples=10)
        X = pd.DataFrame(data[0])
        y = data[1]

        model = autoML.fit(X, y)
        self.assertIsInstance(model.named_steps['estimator'].model, XGBRegressor)

    def test_normal_flow_gscv_regression(self):
        # search_space = [{'estimator': ['catboost'], 'fs__params': [{'max_depth': 5}]}]
        search_space = [{'estimator': ['catboost'],}]
        autoML = AutoML(search = 'GridSearchCV', task = 'regression', models=['catboost'], 
                        search_space = search_space, n_folds=5, feature_selection=True, fs_params = {'max_depth': 5})

        data = make_regression(n_features=30, n_samples=200)


        X = pd.DataFrame(data[0])
        y = data[1]

        before = time.time()
        model = autoML.fit(X, y)
        print("Ran in ", (time.time() - before)/60)
        self.assertIsInstance(model.best_estimator_.named_steps['rgs'], CatBoostRegressorScikit)


    def test_normal_flow_gscv_classification(self):
        search_space = [{'estimator': ['logistic_regression']}]
        autoML = AutoML(search = 'GridSearchCV', task = 'classification', models=['logistic_regression'], 
                        search_space = search_space, n_folds=5, feature_selection=True)

        # titanic_train_df = pd.read_csv("/home/ruteabreu/git/paml/paml/tests/aline.csv")
        # X = titanic_train_df.drop(['performa_30D3M_geo'], axis = 1)
        # y = titanic_train_df['performa_30D3M_geo']

        data = make_classification(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        # data = load_iris()
        # X = data['data']
        # y = data['target']

        before = time.time()
        model = autoML.fit(X, y)
        print("Ran in ", (time.time() - before)/60)
        self.assertIsInstance(model.best_estimator_.named_steps['clf'], LogisticRegression)

        
    def test_flow_gscv_regression_type1(self):
        '''Type 1 - set a search space and a model that is not in the search space'''

        search_space = [{'estimator': ['catboost']}]

        autoML = AutoML(search = 'GridSearchCV', task = 'regression', models=['catboost'], 
                        search_space = search_space, n_folds=5, feature_selection=True)

        data = make_regression(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        model = autoML.fit(X, y)
        self.assertIsInstance(model.best_estimator_.named_steps['rgs'], CatBoostRegressorScikit)

    def test_flow_gscv_regression_type2(self):
        '''Type 2 - set just a model'''
        autoML = AutoML(search = 'GridSearchCV', task = 'regression', models=['catboost'], 
                        n_folds=5)

        data = make_regression(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        model = autoML.fit(X, y)

        return self.assertIsInstance(model.best_estimator_.named_steps['rgs'], CatBoostRegressorScikit)

    def test_flow_gscv_regression_type3(self):
        '''Type 3 - set two or more models without a search_space'''
        autoML = AutoML(search = 'GridSearchCV', task = 'regression', models=['catboost', 'lgbm'], 
                       n_folds=5)

        data = make_regression(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        model = autoML.fit(X, y)
        self.assertIsInstance(model.best_estimator_.named_steps['clf'], CatBoostRegressorScikit) or self.assertIsInstance(model.best_estimator_.named_steps['clf'], LGBMRegressor) 


    def test_flow_gscv_regression_type4(self):
        '''Type 4 - set two or more models with a partially correct search_space'''
        search_space = [{'estimator': ['catboost'],
                        'eval_metric': ["RMSE"],},
                        {'estimator': ['xgb']}]

        autoML = AutoML(search = 'GridSearchCV', task = 'regression', models=['catboost', 'lgbm'], 
                        n_folds=5, search_space= search_space)

        data = make_regression(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        model = autoML.fit(X, y)

    def test_main_flow_optuna_search(self):
        #for testing warning
        search_space = [{'estimator': ['xgb'],
                        'eval_metric': ["AUC"],}]

        autoML = AutoML(search = 'OptunaSearch', task = 'regression', models=['xgb'], feature_selection=True, 
                        search_space = search_space, n_folds=5, n_trials = 1)

        # titanic_train_df = pd.read_csv("/home/ruteabreu/git/automl/paml/tests/train.csv")
        # X = titanic_train_df.drop('Survived', axis = 1)
        # y = titanic_train_df['Survived']

        daniel = pd.read_csv("/home/ruteabreu/git/paml/paml/tests/daniel.csv")
        X = daniel.drop(['RENDAPRESUMIDAPOS'], axis = 1)
        y = daniel['RENDAPRESUMIDAPOS']
       
        # data = make_regression(n_features=4, n_samples=200)
        # X = pd.DataFrame(data[0])
        # y = data[1]
        before = time.time()

        best_model = autoML.fit(X, y)
        print("Run in ", (time.time() - before)/60, 'min')
        return best_model

    def test_main_flow_optuna_search_one_model(self):
        #for testing warning
        autoML = AutoML(search = 'OptunaSearch', task = 'classification', models=['logistic_regression'], 
                       n_folds=5, n_trials = 10, feature_selection=True)

        titanic_train_df = pd.read_csv("/home/ruteabreu/git/paml/paml/tests/aline.csv")
        X = titanic_train_df.drop(['performa_30D3M_geo'], axis = 1)
        y = titanic_train_df['performa_30D3M_geo']


        # df_train = pd.read_csv("/home/ruteabreu/git/automl/paml/tests/2_base_modelagem_interno_2021-11-16.csv")
        # # df_train = df_raw[df_raw['request_dt'] < pd.Timestamp('2021-01-01')]
        # chaves = ['hash_cpf', 'anomes', 'approval_id', 'request_id', 'user_uuid', 'request_dt', 'funnel_status', 'granular_funnel_status']
        # features = ['HSPA', 'age', 'mobile_ddd_state']
        # X = df_train.drop(columns = chaves + ['combination_target'])

        # X = X[features]
        # y = df_train['combination_target']

        # data = make_classification(n_features=40, n_samples=67)
        # X = pd.DataFrame(data[0])
        # y = data[1]

        # data = load_iris()
        # X = data['data']
        # y = data['target']
        
        before = time.time()
        best_model = autoML.fit(X, y)
        print("Run in ", (time.time() - before)/60, 'min')
        return best_model


    def test_all_models_default(self):
        autoML = AutoML(task = 'classification', n_folds=5, n_trials = 1)

        data = make_classification(n_features=4, n_samples=10)
        X = data[0]
        y = data[1]

        best_model = autoML.fit(X, y)
        return best_model